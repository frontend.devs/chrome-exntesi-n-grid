function hexToRgbA(hex) {
  let c
  const rule = /^#([A-Fa-f0-9]{3}){1,2}$/
  if (rule.test(hex)) {
    c = hex.substring(1).split('')
    if (c.length == 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]]
    }
    c = '0x' + c.join('')
    const obj = {}
    obj.r = (c >> 16) & 255
    obj.g = (c >> 8) & 255
    obj.b = c & 255
    return obj
  }
  throw new Error('Bad Hex')
}

;(() => {
  const config = {
    GRID_DEVICE: 37,
    GRID_COLOR: {
      R: 0,
      G: 255,
      B: 255,
      A: 0.18,
    },
  }
  chrome.storage.sync.get('rimac', (data) => {
    const rgba = hexToRgbA(data.rimac.color)

    config.GRID_COLOR.R = rgba.r
    config.GRID_COLOR.G = rgba.g
    config.GRID_COLOR.B = rgba.b
    config.GRID_DEVICE = data.rimac.cols

    const idOverlayName = 'overlayGrid37'
    const styleClassName = 'overlayGrid37'

    const body = document.body
    const div = document.createElement('div')

    const elmHead = document.getElementsByTagName('head')[0]
    const elmStyle = document.createElement('style')

    const selectOverlay = document.querySelector(`.${styleClassName}`)
    const elmOverlay = document.getElementById(idOverlayName)

    const gutter = 100 * (1 / config.GRID_DEVICE).toFixed(8)
    const style = `#${idOverlayName}{margin:auto;pointer-events:none;position:fixed;top:0;bottom:0;right:0;left:0;z-index:9999;width:84%;max-width:480px;background-image:repeating-linear-gradient(90deg, rgba(${config.GRID_COLOR.R}, ${config.GRID_COLOR.G}, ${config.GRID_COLOR.B}, ${config.GRID_COLOR.A}), rgba(${config.GRID_COLOR.R}, ${config.GRID_COLOR.G}, ${config.GRID_COLOR.B}, ${config.GRID_COLOR.A}) ${gutter}%, rgba(255, 255, 255, 0) ${gutter}%, rgba(255, 255, 255, 0) ${
      2 * gutter
    }%)}@media only screen and (min-width:768px){#${idOverlayName}{max-width:592px; width:100%;}}@media only screen and (min-width:1024px){#${idOverlayName}{max-width:925px}}@media only screen and (min-width:1200px){#${idOverlayName}{max-width:1184px}}`

    if (selectOverlay) {
      selectOverlay.remove()
    } else {
      elmStyle.classList.add(styleClassName)
      elmStyle.setAttribute('type', 'text/css')
      elmHead.appendChild(elmStyle)
      if (elmStyle.styleSheet) {
        elmStyle.styleSheet.cssText = style
      } else {
        elmStyle.appendChild(document.createTextNode(style))
      }
    }

    if (elmOverlay) {
      elmOverlay.remove()
    } else {
      div.setAttribute('id', idOverlayName)
      body.appendChild(div)
    }
  })
})()
