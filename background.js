'use strict';

chrome.runtime.onInstalled.addListener(function() {
  chrome.storage.sync.set({
    rimac: {
      cols: '37',
      color: '#ff0000'
    },
  }, function() {
    console.log('config success');
  });

  chrome.declarativeContent.onPageChanged.addRules([{
    conditions: [new chrome.declarativeContent.PageStateMatcher({
      pageUrl: {hostContains: '.'},
    })],
    actions: [new chrome.declarativeContent.ShowPageAction()]
  }]);
});

chrome.browserAction.onClicked.addListener(function() {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.executeScript(
      tabs[0].id, {
        file: 'inject.js',
      }
    );
  });
});

