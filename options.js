// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

let cantGrid = document.getElementById('cant_grid');
let colorGrid = document.getElementById('color_grid');
let message = document.getElementById('message');

document.getElementById("saveForm").addEventListener("click", function(){
  chrome.storage.sync.set({
    rimac: {
      cols: cantGrid.value,
      color: colorGrid.value
    },
  }, function() {
    message.innerText = 'Cambios realizados correctamente'
    message.style.display = 'block'
    setTimeout(() => {
      message.innerText = ''
      message.removeAttribute('style')
    }, 2500);
  });
});


(function () {
  chrome.storage.sync.get('rimac', function (data) {
    cantGrid.setAttribute('value', data.rimac.cols)
    colorGrid.setAttribute('value', data.rimac.color)
  })
}())
